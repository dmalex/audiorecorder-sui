//
//  ContentView.swift
//  AudioRecorder9
//  Created by brfsu on 29.05.2024.
//
import SwiftUI
import AudioUnit
import VoiceRecorderKit

struct ContentView: View {
    @StateObject var recorder = AudioRecorder(numberOfSamples: 2, audioFormatID: kAudioFormatAppleLossless, audioQuality: .max)
        
    var body: some View {
        VStack {
            List {
                ForEach(recorder.recordings, id: \.uid) { recording in
                    VoicePlayerView(audioUrl: recording.fileURL)
                        .onAppear {
                            print(recording.fileURL)
                        }
                }
                .onDelete { indexSet in
                    delete(at: indexSet)
                }
            }
            .listStyle(.inset)
            Spacer()
            VoiceRecorderView(audioRecorder: recorder, recordingCancelled: nil, recordingComplete: nil)
        }
        .onAppear {
            DropView.showSuccess(title: "Hi!")
            recorder.fetchRecordings()
        }
    }
    
    
    func delete(at offsets: IndexSet) {

        var recordingIndex: Int = 0

        for index in offsets {
            recordingIndex = index
        }

        let recording = recorder.recordings[recordingIndex]
        recorder.deleteRecording(url: recording.fileURL, onSuccess: {
            recorder.recordings.remove(at: recordingIndex)
            DropView.showSuccess(title: "Recording removed!")
        })
    }
}
