//
//  AudioRecorder9App.swift
//  AudioRecorder9
//
//  Created by brfsu on 29.05.2024.
//

import SwiftUI

@main
struct AudioRecorder9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
